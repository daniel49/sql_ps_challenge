# Production Support Challenges
## SQL Challenge 1
### Instructions
Import using table import wizard's default settings.

When calling file - **CALL FiveHourVMP('TICKERNAME',UTCTIMESTAMP')**

* 'TICKERNAME' is 4 character ticker i.e AAPL

* 'UTCTIMESTAMP' is a 12 character timestamp in the format YYYYMMDDhhmm i.e 201713312359

###Assumptions


###Validation
Using table import wizard verified max decimal length for open/high/low/close (2DP)

## SQL Challenge 2
### Instructions
Use Create Table SQL file first.
Call file(ch2_script_call) with no parameters- **CALL SQLCH2()**


###Assumptions


###Validation


