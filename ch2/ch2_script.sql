DROP PROCEDURE IF EXISTS SQLCH2;
DELIMITER //
CREATE PROCEDURE SQLCH2()
BEGIN
	-- View with dates formatted
	DROP VIEW IF EXISTS table_date_format;
	CREATE VIEW table_date_format
	AS
	SELECT
		DATE_FORMAT(str_to_date(`tick_date`, '%m/%d/%Y'), '%Y/%m/%d') as tick_date_f,
		time_time,
		t_high,
		t_low,
		t_open,
		t_close,
		ABS(t_open - t_close) as diff
	FROM sql_ch2;

	-- Order largest differences of open/close
	-- Limit to largest 3 dates
	DROP TABLE IF EXISTS largest_Diff; 
	CREATE TEMPORARY TABLE largest_Diff
	SELECT og.*
	FROM table_date_format og
		LEFT JOIN table_date_format comp
			ON og.tick_date_f = comp.tick_date_f AND og.diff < comp.diff
	WHERE comp.diff IS NULL
	GROUP BY tick_date_f
	ORDER BY diff DESC
	LIMIT 3;

	-- Table of highest prices
	DROP TABLE IF EXISTS high_Price;
	CREATE TEMPORARY TABLE high_Price
	SELECT og.*
	FROM table_date_format og
		LEFT JOIN table_date_format comp
			ON og.tick_date_f = comp.tick_date_f AND og.t_high < comp.t_high
	WHERE comp.t_high IS NULL
	ORDER BY t_high DESC;

	-- Combining the highest prices table with the three dates from largest differences
	DROP TABLE IF EXISTS combined_Price_Diff;
	CREATE TABLE combined_Price_Diff
	SELECT ld.tick_date_f, ld.diff, hp.time_time
	FROM largest_Diff ld
		LEFT JOIN high_Price hp 
			ON ld.tick_date_f = hp.tick_date_f
	ORDER BY diff DESC;

	-- Display Result
	SELECT *
	FROM combined_price_diff;
END//
DELIMITER ;







