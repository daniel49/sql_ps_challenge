USE sql_ch1_db;

DROP TABLE sql_ch2;
-- Create table	
CREATE TABLE sql_ch2 (
tick_date VARCHAR(12) NOT NULL,
time_time VARCHAR(4) NOT NULL,
t_open DOUBLE(10,2) NOT NULL,
t_high DOUBLE(10,2) NOT NULL,
t_low DOUBLE(10,2) NOT NULL,
t_close DOUBLE(10,2) NOT NULL,
t_vol BIGINT NOT NULL
-- PRIMARY KEY (tick_date)
);

-- Load Data *Didn't work*
LOAD DATA INFILE '\sample_dataset3.csv'
INTO TABLE sql_ch2
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

SELECT *
FROM sql_ch2;
