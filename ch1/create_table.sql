USE sql_ch1_db

-- Create table	
CREATE TABLE sql_ch1 (
tickerID VARCHAR(4) NOT NULL,
tick_date VARCHAR(12) NOT NULL,
t_open DOUBLE(10,2) NOT NULL,
t_high DOUBLE(10,2) NOT NULL,
t_low DOUBLE(10,2) NOT NULL,
t_close DOUBLE(10,2) NOT NULL,
t_vol INT NOT NULL,
PRIMARY KEY (tickerID)
);

-- Load Data *Didn't work*
LOAD DATA INFILE 'C:\ProgramData\MySQL\MySQL Server 5.7\Uploads\sql_ps_challenge'
INTO TABLE sql_ch1
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;


-- FIVE HOUR INTERVAL + CALC
DELIMITER //
CREATE PROCEDURE FiveHourVWP(IN starttime INT(12))
	BEGIN
	SELECT `<ticker>`, `<date>` as `Start`, `<date>`+500 as `End` ,
	SUM(`<vol>`*`<close>`)/SUM(`<vol>`) as vwp
	FROM sample_dataset2
	WHERE `<date>` BETWEEN 201010110900 AND 201010110900 + 500
	AND `<ticker>` = 'AAPL'
	GROUP BY `<ticker>`;
    END //
DELIMITER ;


-- NEW DATE FORMAT
SELECT *,
str_to_date(`<date>`, '%Y%m%d%H%i') as newdate
FROM sample_dataset2;

ALTER TABLE sample_dataset2
MODIFY `<date>`
INTEGER;

SHOW FIELDS FROM sample_dataset2;
