-- FIVE HOUR INTERVAL + CALC
DROP PROCEDURE IF EXISTS FiveHourVWP;
DELIMITER //
CREATE PROCEDURE FiveHourVWP(IN pickTicker VARCHAR(4), IN startTime VARCHAR(12))
	BEGIN
	SELECT -- `<ticker>`,
		DATE_FORMAT(str_to_date(`<date>`, '%Y%m%d%H%i'), '%d/%m/%Y') as `Date`,
		DATE_FORMAT(str_to_date(`<date>`, '%Y%m%d%H%i'), '%H:%i') as`Start`,
		DATE_FORMAT(str_to_date(`<date>`+500, '%Y%m%d%H%i'), '%H:%i') as `End` ,
		ROUND(SUM(`<vol>`*`<close>`)/SUM(`<vol>`), 5) as `VWP`
	FROM sample_dataset2
	WHERE `<date>` BETWEEN startTime AND startTime + 500
	AND `<ticker>` = pickTicker
	GROUP BY `<ticker>`;
    END //
DELIMITER ;

Call FiveHourVWP('AAPL',201010111400);




